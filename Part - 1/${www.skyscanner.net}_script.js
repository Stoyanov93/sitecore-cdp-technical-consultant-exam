// JavaScript source code
var input = {
	origin: $("#flights-search-controls-root #fsc-origin-search").value,
	destination: $("#flights-search-controls-root #fsc-destination-search").value,
	departTime: $("#flights-search-controls-root #depart-fsc-datepicker-button span").innerText,
	returnTime: $("#flights-search-controls-root #return-fsc-datepicker-button span").innerText,
	personsAndClass: $("#flights-search-controls-root button[name='class-travellers-trigger'] span").innerText,
};

var xhr = new XMLHttpRequest();
xhr.open("POST", "api.capturedata.com", true);
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.send(JSON.stringify(input));