// JavaScript source code
openFile();

function openFile() {
	var input = document.createElement('input');
	input.type = 'file';
	input.onchange = e => {
		var fr = new FileReader();
		fr.onload = function () {
			var jsonObject = parseToObject(fr.result);
			var jsonString = JSON.stringify(jsonObject, null, 4);
			saveFile(jsonString);
		}

		fr.readAsText(e.target.files[0]);
	}

	input.click();
}

function parseToObject(csvText) {
	var result = [];
	var lines = csvText.split("\n");
	// first line
	var columnNames = lines[0].split(",");

	// without the first line
	for (var i = 1; i < lines.length; i++) {
		var currLineData = lines[i].split(",");
		var newItem = {};
		for (var j = 0; j < columnNames.length; j++) {
			//trim white space
			newItem[columnNames[j].trim()] = currLineData[j];
		}
		result.push(newItem);

	}

	// Send result
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "api.capturedata.com", true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(result));

	return result;
}


function saveFile(textData) {
	var a = document.createElement("a");
	var blobData = new Blob([textData], { type: "text/plain" });
	var url = window.URL.createObjectURL(blobData);
	a.href = url;
	a.download = "output.json";
	a.click();
	window.URL.revokeObjectURL(url);
}